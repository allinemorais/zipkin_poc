package com.cadastroUsuario.cadastroUsuario.repository;

import com.cadastroUsuario.cadastroUsuario.model.UsuarioModel;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository  extends CrudRepository<UsuarioModel, Integer> {
}
