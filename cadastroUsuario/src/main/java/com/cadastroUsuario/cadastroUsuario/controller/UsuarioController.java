package com.cadastroUsuario.cadastroUsuario.controller;

import com.cadastroUsuario.cadastroUsuario.model.UsuarioModel;
import com.cadastroUsuario.cadastroUsuario.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioServiceService;

    @PostMapping
    public ResponseEntity<UsuarioModel> salvarCliente(@RequestBody UsuarioModel usuario){

        UsuarioModel usuariomodl = new UsuarioModel();
        try {
            usuariomodl = usuarioServiceService.salvarUsuario(usuario);

        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
        }

        return ResponseEntity.status(201).body(usuario);
    }

}