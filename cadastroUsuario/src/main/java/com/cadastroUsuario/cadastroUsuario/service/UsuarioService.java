package com.cadastroUsuario.cadastroUsuario.service;

import com.cadastroUsuario.cadastroUsuario.model.UsuarioModel;
import com.cadastroUsuario.cadastroUsuario.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class UsuarioService {
    @Autowired
    UsuarioRepository usuarioRepositoryRepository;

    @Autowired
    private CepService cepService;

    public UsuarioModel salvarUsuario(UsuarioModel usuario) {
        HashMap<String, Object> usuarioModel = cepService.buscar(usuario.getCep());
        UsuarioModel usuarioobj = usuarioRepositoryRepository.save(usuario);
        return usuario;
    }

}

