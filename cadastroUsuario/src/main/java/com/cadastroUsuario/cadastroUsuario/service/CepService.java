package com.cadastroUsuario.cadastroUsuario.service;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.cloud.sleuth.annotation.NewSpan;
//import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class CepService {
    @Autowired
    private CepFeign cepFeign;

   @NewSpan(name = "buscar-service")
    public HashMap<String, Object> buscar(@SpanTag("origin") String cep) {
        return cepFeign.buscar(cep);
    }
}