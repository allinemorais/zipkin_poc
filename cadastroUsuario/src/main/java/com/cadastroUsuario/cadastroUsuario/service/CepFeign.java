package com.cadastroUsuario.cadastroUsuario.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.HashMap;

@FeignClient(name = "cotacao", url = "http://localhost:8080")
public interface CepFeign {

    @GetMapping("/{cep}")
    HashMap<String, Object> buscar(@PathVariable String cep);
}